import psycopg2
import re
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import requests
import netaddr

# check_type checks if ip address type is IPv4 or IPv6
def check_type(address):
    if netaddr.valid_ipv4(address) is True:
        return("IPv4")
    else:
        if netaddr.valid_ipv6(address) is True:
            return("IPv6")

# scrape function make list of addresses  
def scrape(url):
    
    page = requests.get(url).text.split("\n")
    del page[-1]
    return page

# create_database function create database and tables if they not exists 
def create_database():

    # create connection
    con = psycopg2.connect("user=postgres dbname=postgres host=localhost")
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);

    cursor = con.cursor();

    cursor.execute("SELECT datname FROM pg_database;")
    name_database   = "redamp";

    # Check if database exists or not 
    list_database = cursor.fetchall()
    if (name_database,) in list_database:
        print("database already exist")
    else:
        sql_create_database = "create database "+name_database+";"
        cursor.execute(sql_create_database);
    cursor.close()
    con.close()


def create_tables():
    # commands to create tables 
    con = psycopg2.connect("dbname=redamp user=postgres host=localhost")
    cursor = con.cursor();
    commands = (
        """
            CREATE TABLE IF NOT EXISTS source (
            name VARCHAR(15) PRIMARY KEY
        )""",
        """
        CREATE TABLE IF NOT EXISTS IPs (
            id SERIAL PRIMARY KEY,
            ip VARCHAR(25),
            type VARCHAR(15),
            source VARCHAR(15),
            FOREIGN KEY (source) REFERENCES source(name)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS URLs (
            id SERIAL PRIMARY KEY,
            URL VARCHAR UNIQUE,
            source VARCHAR(15),
            FOREIGN KEY (source) REFERENCES source(name)
        )
        """)
    try:
        # execute each command from list of commands
        for command in commands:
            cursor.execute(command)
        # Create unique index to avoid duplicates on insert 
        cursor.execute("CREATE UNIQUE INDEX ip_index ON IPs(ip,source)")
        cursor.close()
        con.commit()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if con is not None:
            con.close()

# insert_to_database inserts data to database  
def insert_to_database():

    badips = scrape("https://www.badips.com/get/list/any/2")
    openphish = scrape("https://openphish.com/feed.txt")

    # commands for insert to each table 

    sql_url = """INSERT INTO URLs(URL, source)
                VALUES(%s,%s) ON CONFLICT(URL) DO NOTHING;
            """

    sql = """INSERT INTO IPs(ip, type, source)
                VALUES(%s,%s,%s) ON CONFLICT(ip,source) DO NOTHING;
            """
    sql_source = """INSERT INTO source(name)
                    VALUES(%s) ON CONFLICT DO NOTHING;
                """ 
            
    try:
        con = psycopg2.connect("dbname=redamp user=postgres host=localhost")
        # create a new cursor
        cur = con.cursor()

        cur.execute(sql_source, ("Badips",))
        cur.execute(sql_source, ("Reputation",))
        cur.execute(sql_source, ("Openphish",))

        for i in openphish: 
            cur.execute(sql_url, (i,"Openphish",))
        for i in badips: 
            ip_type = check_type(i)
            cur.execute(sql, (i,ip_type,"Badips",))

        url3 = "http://reputation.alienvault.com/reputation.data"
        page3 = requests.get(url3).text
        for line in page3.splitlines():
            # regex to match only Ip adress and nothing else
            matchobj = re.match(r'^(.*?)\#.*', line,re.M|re.I)
            if matchobj:
                ip_type = check_type(matchobj.group(1))
                cur.execute(sql, (matchobj.group(1), ip_type,"Reputation",))
            else:
                print ("No match!!")

        con.commit()
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    
    finally:
        if con is not None:
            con.close()
