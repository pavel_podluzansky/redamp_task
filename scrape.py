from core.database_operations import *

def main():
    create_database()
    create_tables()
    insert_to_database()

if __name__ == "__main__":
    main()